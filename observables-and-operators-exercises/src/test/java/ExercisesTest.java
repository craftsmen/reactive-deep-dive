import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;
import nl.craftsmen.workshop.IsPendingTransformer;
import nl.craftsmen.workshop.KeepAliveObservable;
import nl.craftsmen.workshop.SumObservable;
import nl.craftsmen.workshop.TrafficTicketsTransformer;
import nl.craftsmen.workshop.domain.SpeedingPenalty;
import nl.craftsmen.workshop.domain.TrafficCameraRecord;
import nl.craftsmen.workshop.util.KeepAlive;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class ExercisesTest {

    @Test
    public void testSumObservable() {

        // TODO: implement the SumObservable operator so it returns the sum of all the Integers it gets

        Observable.just(1,2,3,4)
                .compose(SumObservable::new)
                .test()
                .assertValueCount(1)
                .assertValue(10)
                .assertComplete();

        Observable.range(1, 100)
                .compose(SumObservable::new)
                .test()
                .assertValueCount(1)
                .assertValue(5050)
                .assertComplete();

    }

    @Test
    public void testIsPendingObservable() {

        // TODO: implement the IsPendingTransformer operator so that it returns true so long the previous Observable
        // has not emitted anything yet, after that returns false

        TestScheduler testScheduler = new TestScheduler();
        Observable<Long> intervalObservable = Observable.interval(2l, TimeUnit.SECONDS, testScheduler);
        TestObserver<Boolean> testSubscriber = new TestObserver<>();

        intervalObservable
                .compose(new IsPendingTransformer())
                .subscribe(testSubscriber);

        testSubscriber.assertValueCount(1);
        testSubscriber.assertValues(true);
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS);
        testSubscriber.assertValueCount(1);
        testSubscriber.assertValues(true);
        testScheduler.advanceTimeBy(1, TimeUnit.SECONDS);
        testSubscriber.assertValueCount(2);
        testSubscriber.assertValues(true, false);

    }

    @Test
    public void testKeepAliveObservable() {

        // RxJava uses daemon threads which does not prevent the java app from exiting.
        // We want to implement the KeepAliveObservable that will count the number of subscribers to the Observable chain.
        // Whenever the number of subscribers is greater then 0 we will spawn a user thread so to prevent the app from exiting when
        // there are still subscribers.
        //
        // In the util package you can find a KeepAlive utility class that has 2 public methods increment and decrement.
        // It has an inner counter and whenever the counter is greater then 0 it will spawn a thread and will keep it running till the
        // counter hits 0 again.
        //
        // Implement the desired functionality in the KeepAliveObserver and dont forget to implement the subscribeActual

        TestScheduler testScheduler = new TestScheduler();
        Observable<Long> intervalObservable = Observable.interval(0l, 3l, TimeUnit.SECONDS, testScheduler);
        TestObserver<Long> testSubscriber = new TestObserver<>();

        assertTrue(KeepAlive.getNumberOfSubscribers() == 0);

        intervalObservable
                .compose(KeepAliveObservable::new)
                .subscribe(testSubscriber);

        assertTrue(KeepAlive.getNumberOfSubscribers() == 1);

        Observable.just(1,2,3,4)
                .compose(KeepAliveObservable::new)
                .subscribe();

        assertTrue(KeepAlive.getNumberOfSubscribers() == 1);

        testSubscriber.dispose();

        assertTrue(KeepAlive.getNumberOfSubscribers() == 0);

    }

    @Test
    public void testSpeedingPenaltiesObservable() {

        // TODO: implement the TrafficTicketsTransformer so it transforms a TrafficCameraRecord Observable stream to an SpeedingPenalty Observable Stream
        // a TrafficCameraRecord has a car's licensePlateNumber and the speed recorded
        // a SpeedingPenalty also has a car's licensePlateNumber and the amount the owner must pay if he exceeded the speedLimit
        // the amount to pay is equal to actualSpeed - speedLimit
        // for each offence emit a fine, if the same car commits an offence more then once emit the fine with the accumulated fine amount.
        // you might want to use the PenaltiesGathererObservable, defined within the Transformer.


        final Integer SPEED_LIMIT = 100;

        Observable<TrafficCameraRecord> trafficCameraRecord$ = Observable.just(
                new TrafficCameraRecord("11", 160),
                new TrafficCameraRecord("22", 90),
                new TrafficCameraRecord("33", 120),
                new TrafficCameraRecord("11", 130),
                new TrafficCameraRecord("11", 130),
                new TrafficCameraRecord("11", 130),
                new TrafficCameraRecord("33", 120)
        );

        trafficCameraRecord$
                .compose(new TrafficTicketsTransformer(SPEED_LIMIT))
                .test()
                .assertValueCount(6)
                .assertValues(
                        new SpeedingPenalty("11", 60),
                        new SpeedingPenalty("33", 20),
                        new SpeedingPenalty("11", 90),
                        new SpeedingPenalty("11", 120),
                        new SpeedingPenalty("11", 150),
                        new SpeedingPenalty("33", 40)
                )
                .assertComplete();

    }

}
