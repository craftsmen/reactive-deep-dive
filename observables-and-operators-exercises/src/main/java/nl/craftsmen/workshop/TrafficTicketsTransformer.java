package nl.craftsmen.workshop;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Observer;
import nl.craftsmen.workshop.domain.SpeedingPenalty;
import nl.craftsmen.workshop.domain.TrafficCameraRecord;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class TrafficTicketsTransformer implements ObservableTransformer<TrafficCameraRecord, SpeedingPenalty> {

    //==================================================================================================================
    //
    // Implement the TrafficTicketsTransformer so it transforms a TrafficCameraRecord Observable stream to an SpeedingPenalty Observable Stream
    // a TrafficCameraRecord has a car's licensePlateNumber and the speed recorded
    // a SpeedingPenalty also has a car's licensePlateNumber and the amount the owner must pay if he exceeded the speedLimit
    // the amount to pay is equal to actualSpeed - speedLimit
    // for each offence emit a fine, if the same car commits an offence more then once emit the fine with the accumulated fine amount.
    // you might want to use the PenaltiesGathererObservable, defined within the Transformer.
    //
    //==================================================================================================================

    private Integer speedLimit;

    public TrafficTicketsTransformer(Integer speedLimit) {
        this.speedLimit = speedLimit;
    }

    @Override
    public ObservableSource<SpeedingPenalty> apply(Observable<TrafficCameraRecord> upstream) {
        return null;
    }

    class PenaltiesGathererObservable extends Observable<SpeedingPenalty> {

        Observable<SpeedingPenalty> source;
        Map<String, SpeedingPenalty> penaltiesList = new TreeMap<>();

        public PenaltiesGathererObservable(Observable<SpeedingPenalty> source) {
            this.source = source;
        }

        @Override
        protected void subscribeActual(Observer<? super SpeedingPenalty> observer) {
            source.subscribe(
                    (v) -> {
                        penaltiesList.merge(v.getLicensePlateNumber(), v,
                                (x, y) -> new SpeedingPenalty(x.getLicensePlateNumber(), x.getAmount() + y.getAmount()));
                        observer.onNext(penaltiesList.get(v.getLicensePlateNumber()));
                    },
                    (e) -> observer.onError(e),
                    () -> {
                        penaltiesList = Collections.emptyMap();
                        observer.onComplete();
                    }
            );
        }
    }
}
