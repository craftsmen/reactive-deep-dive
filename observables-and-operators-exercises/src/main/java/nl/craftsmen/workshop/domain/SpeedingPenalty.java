package nl.craftsmen.workshop.domain;

import java.util.Objects;

public class SpeedingPenalty {
    private String licensePlateNumber;
    private Integer amount;

    public SpeedingPenalty(String licensePlateNumber, Integer amount) {
        this.licensePlateNumber = licensePlateNumber;
        this.amount = amount;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpeedingPenalty that = (SpeedingPenalty) o;
        return Objects.equals(licensePlateNumber, that.licensePlateNumber) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public String toString() {
        return "SpeedingPenalty{" +
                "licensePlateNumber='" + licensePlateNumber + '\'' +
                ", amount=" + amount +
                '}';
    }
}
