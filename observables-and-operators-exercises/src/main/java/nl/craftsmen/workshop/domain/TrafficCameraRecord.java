package nl.craftsmen.workshop.domain;

public class TrafficCameraRecord {
    private String licensePlateNumber;
    private Integer speed;

    public TrafficCameraRecord(String licensePlateNumber, Integer speed) {
        this.licensePlateNumber = licensePlateNumber;
        this.speed = speed;
    }

    public String getLicensePlateNumber() {
        return licensePlateNumber;
    }

    public Integer getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return "TrafficCameraRecord{" +
                "licensePlateNumber='" + licensePlateNumber + '\'' +
                ", speed=" + speed +
                '}';
    }
}
