package nl.craftsmen.workshop;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;

public class IsPendingTransformer<T> implements ObservableTransformer<T, Boolean> {

    //==================================================================================================================
    //
    // Implement the IsPendingTransformer Transformer Observable so that it returns true so long the previous Observable
    // has not emitted anything yet, after the first emit return false.
    //
    // =================================================================================================================

    @Override
    public ObservableSource<Boolean> apply(Observable<T> upstream) {
        return null;
    }
}
