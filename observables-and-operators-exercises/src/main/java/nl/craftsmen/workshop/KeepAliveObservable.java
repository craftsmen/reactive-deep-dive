package nl.craftsmen.workshop;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import nl.craftsmen.workshop.util.KeepAlive;

public class KeepAliveObservable<T> extends Observable<T> {

    //==================================================================================================================
    //
    // RxJava uses daemon threads which does not prevent the java app from exiting.
    // We want to implement the KeepAliveObservable that will count the number of subscribers to the Observable chain.
    // Whenever the number of subscribers is greater then 0 we will spawn a user thread so to prevent the app from exiting when
    // there are still subscribers.
    //
    // In the util package you can find a KeepAlive utility class that has 2 public methods increment and decrement.
    // It has an inner counter and whenever the counter is greater then 0 it will spawn a thread and will keep it running till the
    // counter hits 0 again.
    //
    // Implement the desired functionality in the KeepAliveObserver and dont forget to implement the subscribeActual
    //
    //==================================================================================================================

    private final ObservableSource<T> source;

    public KeepAliveObservable(ObservableSource<T> source) {
        this.source = source;
    }

    @Override
    protected void subscribeActual(Observer<? super T> observer) {

    }

    private static class KeepAliveObserver<T> implements Observer<T>, Disposable {

        private final Observer<? super T> downstream;

        private Disposable upstream;

        public KeepAliveObserver(Observer<? super T> downstream) {
            this.downstream = downstream;
        }

        @Override
        public void onSubscribe(Disposable d) {
            this.upstream = d;
        }

        @Override
        public void onNext(T t) {
            downstream.onNext(t);
        }

        @Override
        public void onError(Throwable e) {
            downstream.onError(e);
        }

        @Override
        public void onComplete() {
            downstream.onComplete();
        }


        @Override
        public void dispose() {
            this.upstream.dispose();
        }

        @Override
        public boolean isDisposed() {
            return this.upstream.isDisposed();
        }
    }

}
