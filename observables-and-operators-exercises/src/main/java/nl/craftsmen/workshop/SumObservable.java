package nl.craftsmen.workshop;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SumObservable extends Observable<Integer> {

    //==================================================================================================================
    //
    //  Implement the SumObservable<Integer> observable so it returns the sum of all the Integers it gets from the source.
    //  (exactly like the sum() operator in the streaming API)
    //
    // =================================================================================================================

    Observable<Integer> source;

    public SumObservable(Observable<Integer> source) {
        this.source = source;
    }

    @Override
    protected void subscribeActual(Observer observer) {

    }

    private static class SumObserver implements Observer<Integer>, Disposable {

        private final Observer<Integer> downstream;

        private Disposable upstream;

        public SumObserver(Observer<Integer> downstream) {
            this.downstream = downstream;
        }

        @Override
        public void onSubscribe(Disposable d) {
            this.upstream = d;
        }

        @Override
        public void onNext(Integer i) {
            // TODO: implement this
        }

        @Override
        public void onError(Throwable e) {
            downstream.onError(e);

        }

        @Override
        public void onComplete() {
            // TODO: implement this
        }

        @Override
        public void dispose() {
            this.upstream.dispose();
        }

        @Override
        public boolean isDisposed() {
            return this.upstream.isDisposed();
        }
    }
}
