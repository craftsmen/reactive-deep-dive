# Observables & Operators

## Instructions
Complete the implementation of the Observables in nl.craftsmen.workshop.
Instruction about how and what to implement are in the Observable class itself.

## Test
Run the tests to check if your implementation is correct.