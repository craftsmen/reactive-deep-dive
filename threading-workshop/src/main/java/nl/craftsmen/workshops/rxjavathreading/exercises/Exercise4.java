package nl.craftsmen.workshops.rxjavathreading.exercises;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import nl.craftsmen.workshops.rxjavathreading.util.ExampleStreams;
import nl.craftsmen.workshops.rxjavathreading.util.KeepAliveObservable;
import nl.craftsmen.workshops.rxjavathreading.util.WorkshopUtils;

public class Exercise4 implements Runnable {

    public static void main(String[] args) {
        new Exercise4().run();
    }

    @Override
    public void run() {

        //==================================================================================================================================
        //
        // EXERCISE 4
        //
        // In the previous exercise we have used the `WorkshopUtils.computeGeometricMean` which performs a (simulated) CPU intensive
        // computation. In fact this computation is much slower than the rate at which the sensor data is received. As can be soon from the
        // output of the previous exercise: only one computation thread was used. This raises the question if we can speed up the data
        // processing by performing the computation on multiple threads.
        //
        // ASSIGNMENT: Copy your solution from the previous exercise to once again obtain a new stream `meanSensorData` and modify the
        // solution to parallelize the geometric mean computation using the computation scheduler.

        var sensorData = ExampleStreams.highFrequencySensorData();

        var meanSensorData = sensorData; // ???

        // EXPECTED OUTPUT: You should see 10 values which are emitted on multiple RxComputationThreadPool-x threads. Also the values are
        // printed out much faster compared to the output of exercise 3.

        meanSensorData
                .take(10)
                .compose(KeepAliveObservable::new)
                .subscribe(WorkshopUtils.consoleLogger());
    }

}
