package nl.craftsmen.workshops.rxjavathreading;

import nl.craftsmen.workshops.rxjavathreading.exercises.*;

import java.util.Map;

public class ExerciseLauncher {

    private static Map<String, Runnable> exerciseMap = Map.ofEntries(
            Map.entry("1", new Exercise1()),
            Map.entry("2", new Exercise2()),
            Map.entry("3", new Exercise3()),
            Map.entry("4", new Exercise4()),
            Map.entry("5", new Exercise5())
    );

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("\u001b[31mUsage: gradlew run <exercise>\u001b[0m");
            return;
        }

        var exerciseKey = args[0];

        if (!exerciseMap.containsKey(exerciseKey)) {
            System.out.println("\u001b[31mUnknown exercise " + exerciseKey + "\u001b[0m");
            return;
        }

        System.out.println("\u001b[32;1mStarting exercise " + exerciseKey  + "...\u001b[0m\n");

        exerciseMap.get(exerciseKey).run();
    }

}
