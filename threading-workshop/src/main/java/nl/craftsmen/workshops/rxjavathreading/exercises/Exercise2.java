package nl.craftsmen.workshops.rxjavathreading.exercises;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import nl.craftsmen.workshops.rxjavathreading.util.ExampleStreams;
import nl.craftsmen.workshops.rxjavathreading.util.KeepAliveObservable;
import nl.craftsmen.workshops.rxjavathreading.util.WorkshopUtils;

public class Exercise2 implements Runnable {

    public static void main(String[] args) {
        new Exercise2().run();
    }

    @Override
    public void run() {

        //==================================================================================================================================
        //
        // EXERCISE 2
        //
        // In this exercise you are provided with two streams of sensor data (`sensorData1` and `sensorData2`) from two different devices.
        //
        // ASSIGNMENT: Create a new stream `averagedSensorData` that emits the average of the last emitted values of the two sensor data
        // streams. Make sure both stream (which are still doing blocking I/O) perform their work on the I/O scheduler.
        //
        // HINT: An example marble diagram for the desired solution is shown below:
        //
        //  * sensorData1:        --[3]-[4]-----------------------------[8]----------------------------[2]-----------------
        //  * sensorData2:        --------------[2]--------[6]-------------------------------------[2]---------------------
        //  * averagedSensorData: --------------[3]--------[5]----------[7]------------------------[5]-[2]-----------------
        //
        // HINT: For the following exercises you do not need to make use of `WorkshopUtils.sleep` function to keep the application alive.
        // Instead we will be using the `KeepAliveObservable` observable, that will take care of this.

        var sensorData1 = ExampleStreams.sensorData();
        var sensorData2 = ExampleStreams.sensorData();

        var averagedSensorData = sensorData1; // + sensorData2 ???

        // EXPECTED OUTPUT: You should see 10 values which are emitted on the RxCachedThreadScheduler-1 or -2 thread.

        averagedSensorData
                .take(10)
                .compose(KeepAliveObservable::new)
                .subscribe(WorkshopUtils.consoleLogger());
    }

}