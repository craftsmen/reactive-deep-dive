package nl.craftsmen.workshops.rxjavathreading.exercises;

import io.reactivex.schedulers.Schedulers;
import nl.craftsmen.workshops.rxjavathreading.util.ExampleStreams;
import nl.craftsmen.workshops.rxjavathreading.util.KeepAliveFlowable;
import nl.craftsmen.workshops.rxjavathreading.util.WorkshopUtils;

public class Exercise5 implements Runnable {

    public static void main(String[] args) {
        new Exercise5().run();
    }

    @Override
    public void run() {

        //==================================================================================================================================
        //
        // EXERCISE 5.A
        //
        // In this exercise you are given yet another sensor data stream which starts emitting data at an increasing pace. This pace will
        // quickly grow to thousands of emits per second. In contrast to previous exercises you will be working with sensor data that is
        // represented using a `Flowable` instead of an `Observable`.
        //
        // ASSIGNMENT: Just run the exercise without making any changes to see for yourself that the sensor data is emitted at a very high
        // rate.

        var sensorData = ExampleStreams.veryHighFrequencySensorData();

        var sensorDataHash = sensorData.subscribeOn(Schedulers.io()); // ???

        sensorDataHash
                .compose(KeepAliveFlowable::new)
                .subscribe(WorkshopUtils.consoleLogger());


        //==================================================================================================================================
        //
        // EXERCISE 5.B
        //
        // This time we are going to process the sensor data by creating hashes of chunks sensor readings.
        //
        // ASSIGNMENT: Modify the sensorDataHash stream such that it starts creating chunks of 10 sensor readings and then applies the
        // hashing function (`WorkshopUtils.computeHash`). Since the hashing function is a heavy operation for the CPU, make sure this work
        // is performed using the computation scheduler (doesn't need to be parallelized).


        // (modify the `sensorDataHash` stream above and rerun the exercise)


        // EXPECTED OUTPUT: You should see about 7 hashes being emitted on the RxComputationThreadPool-1 thread followed by an
        // `MissingBackpressureException`.



        //==================================================================================================================================
        //
        // EXERCISE 5.C
        //
        // By now you should have seen that the sensor data is produced much faster than the CPU is able to compute hashes, eventually
        // leading to the `MissingBackpressureException`. That error is thrown because the backpressure strategy for the stream has been
        // set to throw an error. Let's see what would have happened if we wouldn't have thought of backpressure and modeled the sensor data
        // stream as an `Observable` instead of a `Flowable`.
        //
        // ASSIGNMENT: Modify the `sensorDataHash` by setting the backpressure strategy to an unbounded buffer. This mimics the behavior
        // of observables (without having to convert the `Flowable` stream back to an `Observable`).


        // (modify the `sensorDataHash` stream above and rerun the exercise)


        // EXPECTED OUTPUT: You should see a few hashes (20 to 30, depending on your machine) being emitted. Eventually the application
        // either terminates with an `OutOfMemoryError` or it comes to grinding halt where your CPU usage is rising to 100% and memory
        // consumption spikes.



        //==================================================================================================================================
        //
        // EXERCISE 5.D
        //
        // As you can see: ignoring backpressure can lead to serious issues in your application. Which backpressure strategy works best
        // depends on the context and there no one size fits all solution. In this exercise, however, it is acceptable to drop old values
        // and keep track of the most recent emitted sensor readings.
        //
        // ASSIGNMENT: Modify the `sensorDataHash` by setting the backpressure strategy to keep the lastest emitted values.

        // (modify the `sensorDataHash` stream above and rerun the exercise)

        // EXPECTED OUTPUT: A stream of hashes being emitted at a steady rate. The stream is unbounded so it will keep on printing hashes
        // until the program is terminated manually.
    }

}
