package nl.craftsmen.workshops.rxjavathreading.util;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.Collection;

public final class WorkshopUtils {

    private WorkshopUtils() { }

    public static void sleep(long delay) {
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> ConsoleLogger<T> consoleLogger() {
        return new ConsoleLogger<>(String::valueOf);
    }

    public static double computeGeometricMean(Collection<Double> values) {
        var product = 1.0;

        for (var value : values) {
            product *= value;
        }

        var result = Math.pow(product, 1.0 / values.size());

        try {
            Thread.sleep((long)((Math.random() + 1.5) * 1000));
        } catch (InterruptedException e) { }

        return result;
    }

    public static long computeHash(Collection<Double> values) {
        var hash = 0L;

        for (var value : values) {
            hash = Long.rotateRight(hash, 3) ^ value.hashCode();
        }

        try {
            Thread.sleep((long)((Math.random() + 1.5) * 1000));
        } catch (InterruptedException e) { }

        return hash;
    }

    public static class ConsoleLogger<T> implements Observer<T>, Subscriber<T> {

        private final Function<T, String> formatter;
        private int numberOfEvents = 0;
        private long startTime;
        private Subscription subscription;

        public ConsoleLogger(Function<T, String> formatter) {
            this.formatter = formatter;
        }

        @Override
        public void onSubscribe(Disposable s) {
            startTime = System.currentTimeMillis();
        }

        @Override
        public void onSubscribe(Subscription s) {
            this.subscription = s;

            startTime = System.currentTimeMillis();

            subscription.request(1);
        }

        @Override
        public void onNext(T value) {
            printHeader();

            String formattedValue;

            try {
                formattedValue = formatter.apply(value);
            } catch (RuntimeException e) {
                throw e;
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }

            printEvent(++numberOfEvents, elapsedTime(), "NEXT", formattedValue);

            if (subscription != null) {
                subscription.request(1);
            }
        }

        @Override
        public void onError(Throwable error) {
            printHeader();
            printEvent(++numberOfEvents, elapsedTime(), "ERROR", error.toString());
        }

        @Override
        public void onComplete() {
            printHeader();
            printEvent(++numberOfEvents, elapsedTime(), "COMPLETE", "---");
        }

        private void printHeader() {
            if (numberOfEvents > 0) {
                return;
            }

            System.out.println("   # |     TIME | EVENT    | THREAD                       | VALUE                   ");
            System.out.println("-----+----------+----------+------------------------------+-------------------------");
        }

        private void printEvent(int eventNumber, long elapsedTime, String eventType, String eventValue) {
            System.out.printf(
                    " %3d | %8.2f | %-8s | %-28s | %s\n",
                    eventNumber,
                    elapsedTime / 1000.0,
                    eventType,
                    Thread.currentThread().getName(),
                    eventValue
            );
        }

        private long elapsedTime() {
            return System.currentTimeMillis() - startTime;
        }
    }

}
