package nl.craftsmen.workshops.rxjavathreading.util;

import java.util.concurrent.atomic.AtomicLong;

public final class KeepAlive {

    private static KeepAlive instance = new KeepAlive();

    private AtomicLong keepAliveCounter = new AtomicLong();

    private final Object exitSignal = new Object();

    private AtomicLong keepAliveThreadNumber = new AtomicLong();

    private synchronized void doIncrement() {
        var keepAliveCount = keepAliveCounter.incrementAndGet();

        if (keepAliveCount == 1) {
            startKeepAliveThread();
        }
    }

    private synchronized void doDecrement() {
        var keepAliveCount = keepAliveCounter.decrementAndGet();

        if (keepAliveCount < 0) {
            throw new RuntimeException("Keep alive counter has been decreased below 0");
        }

        if (keepAliveCount == 0) {
            stopKeepAliveThread();
        }
    }

    private void startKeepAliveThread() {
        var threadName = "KeepAliveThread-" + keepAliveThreadNumber.incrementAndGet();

        var keepAliveThread = new Thread(() -> {
            try {
                synchronized (exitSignal) {
                    exitSignal.wait();
                }
            } catch (InterruptedException e) { }
        }, threadName);

        keepAliveThread.start();
    }

    private void stopKeepAliveThread() {
        synchronized (exitSignal) {
            exitSignal.notify();
        }
    }

    public static void increment() {
        instance.doIncrement();
    }

    public static void decrement() {
        instance.doDecrement();
    }

}
