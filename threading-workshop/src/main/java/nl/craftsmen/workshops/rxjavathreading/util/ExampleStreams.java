package nl.craftsmen.workshops.rxjavathreading.util;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public final class ExampleStreams {

    private static Long TIMER_RESOLUTION = 16L;

    private ExampleStreams() { }

    public static Observable<Double> sensorData() {
        return sensorData(count -> (Math.random() * 1000));
    }

    public static Observable<Double> highFrequencySensorData() {
        return sensorData(count -> (Math.random() * 100));
    }

    public static Flowable<Double> veryHighFrequencySensorData() {
        return sensorData(count -> (Math.random() * 1500 * Math.pow(0.2, Math.log10(count + 1)))).toFlowable(BackpressureStrategy.ERROR);
    }

    private static Observable<Double> sensorData(Function<Long, Double> getDelay) {
        return Observable.create(emitter -> {

            var count = 0L;
            var carryOverDelay = 0.0;

            // Loop while the subscriber has not unsubscribed yet.
            try {
                while (!emitter.isDisposed()) {
                    var delay = carryOverDelay + getDelay.apply(count++);
                    carryOverDelay = delay;

                    var slept = false;
                    if (delay >= TIMER_RESOLUTION) {
                        Thread.sleep((long) delay);
                        carryOverDelay -= (long) delay;
                        slept = true;
                    }

                    if (!emitter.isDisposed()) {
                        var iterations = slept ? 1 : (int) Math.min(5, Math.floor(TIMER_RESOLUTION / carryOverDelay));

                        if (!slept) {
                            carryOverDelay = TIMER_RESOLUTION - iterations * carryOverDelay;
                        }

                        for (var i = 0; i < iterations; i++) {
                            emitter.onNext(Math.random());
                        }
                    }
                }
            } catch (InterruptedException e) { }

            emitter.onComplete();

        });
    }


}
