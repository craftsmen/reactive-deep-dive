package nl.craftsmen.workshops.rxjavathreading.util;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public class KeepAliveFlowable<T> extends Flowable<T> {

    private final Publisher<T> source;

    public KeepAliveFlowable(Publisher<T> source) {
        this.source = source;
    }

    @Override
    protected void subscribeActual(Subscriber<? super T> subscriber) {
        var keepAliveSubscriber = new KeepAliveSubscriber<T>(subscriber);

        source.subscribe(keepAliveSubscriber);

        subscriber.onSubscribe(keepAliveSubscriber);
    }

    private static class KeepAliveSubscriber<T> implements Subscriber<T>, Subscription {

        private final Subscriber<? super T> downstream;

        private Subscription upstream;

        public KeepAliveSubscriber(Subscriber<? super T> downstream) {
            this.downstream = downstream;
        }

        @Override
        public void onSubscribe(Subscription s) {
            this.upstream = s;

            KeepAlive.increment();
        }

        @Override
        public void onNext(T t) {
            downstream.onNext(t);
        }

        @Override
        public void onError(Throwable e) {
            downstream.onError(e);

            KeepAlive.decrement();
        }

        @Override
        public void onComplete() {
            downstream.onComplete();

            KeepAlive.decrement();
        }


        @Override
        public void cancel() {
            this.upstream.cancel();

            KeepAlive.decrement();
        }

        @Override
        public void request(long n) {
            this.upstream.request(n);
        }
    }
}
