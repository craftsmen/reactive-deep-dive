package nl.craftsmen.workshops.rxjavathreading.util;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class KeepAliveObservable<T> extends Observable<T> {

    private final ObservableSource<T> source;

    public KeepAliveObservable(ObservableSource<T> source) {
        this.source = source;
    }

    @Override
    protected void subscribeActual(Observer<? super T> observer) {
        var keepAliveObserver = new KeepAliveObserver<T>(observer);

        observer.onSubscribe(keepAliveObserver);

        source.subscribe(keepAliveObserver);
    }

    private static class KeepAliveObserver<T> implements Observer<T>, Disposable {

        private final Observer<? super T> downstream;

        private Disposable upstream;

        public KeepAliveObserver(Observer<? super T> downstream) {
            this.downstream = downstream;
        }

        @Override
        public void onSubscribe(Disposable d) {
            this.upstream = d;

            KeepAlive.increment();
        }

        @Override
        public void onNext(T t) {
            downstream.onNext(t);
        }

        @Override
        public void onError(Throwable e) {
            downstream.onError(e);

            KeepAlive.decrement();
        }

        @Override
        public void onComplete() {
            downstream.onComplete();

            KeepAlive.decrement();
        }


        @Override
        public void dispose() {
            this.upstream.dispose();

            KeepAlive.decrement();
        }

        @Override
        public boolean isDisposed() {
            return this.upstream.isDisposed();
        }
    }
}
