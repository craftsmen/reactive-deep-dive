package nl.craftsmen.workshops.rxjavathreading.exercises;

import io.reactivex.schedulers.Schedulers;
import nl.craftsmen.workshops.rxjavathreading.util.ExampleStreams;
import nl.craftsmen.workshops.rxjavathreading.util.KeepAliveObservable;
import nl.craftsmen.workshops.rxjavathreading.util.WorkshopUtils;

public class Exercise3 implements Runnable {

    public static void main(String[] args) {
        new Exercise3().run();
    }

    @Override
    public void run() {

        //==================================================================================================================================
        //
        // EXERCISE 3
        //
        // We are once again going to process sensor data. This time for a sensor that produces reading at a higher frequency. Also the
        // processing of the data is done using a (simulated) CPU intensive operation.
        //
        // ASSIGNMENT: Create a new `meanSensorData` stream based on `sensorData` which takes every 10 subsequently emitted sensor readings
        // and computes the geometric mean using the static `WorkshopUtils.computeGeometricMean` function. Although, computing the mean is
        // a lightweight task, the function has been modified to simulate heavy CPU load. Therefore your solution should make sure that the
        // computation scheduler is used. Furthermore make sure that the sensor data is being generated on the I/O scheduler.
        //
        // HINT: Use the `buffer` operator to get slices of subsequent sensor data readings.

        var sensorData = ExampleStreams.highFrequencySensorData();

        var meanSensorData = sensorData; // ???

        // EXPECTED OUTPUT: You should see 10 values which are emitted on the RxComputationThreadPool-1 thread.

        meanSensorData
                .take(10)
                .compose(KeepAliveObservable::new)
                .subscribe(WorkshopUtils.consoleLogger());
    }

}
