package nl.craftsmen.workshops.rxjavathreading.exercises;

import io.reactivex.schedulers.Schedulers;
import nl.craftsmen.workshops.rxjavathreading.util.ExampleStreams;
import nl.craftsmen.workshops.rxjavathreading.util.WorkshopUtils;

public class Exercise1 implements Runnable {

    public static void main(String[] args) {
        new Exercise1().run();
    }

    @Override
    public void run() {

        //==================================================================================================================================
        //
        // EXERCISE 1.A
        //
        // The `sensorData` stream below represents an observable that emits (simulated) live sensor data that is received from a remote
        // device.
        //
        // ASSIGNMENT: Subscribe to the `sensorData` stream below and print the data to the console. Only print out the first 10 values
        // that were received from the remote sensor device.
        //
        // HINT: For the workshop we provide a observer that prints all events, their sequence number and timing to the console. You can
        // create a such an observer using the static `WorkshopUtils.consoleLogger()` function.

        var sensorData = ExampleStreams.sensorData();

        // sensorData.???.subscribe(???)

        System.out.println("Done!");

        // EXPECTED OUTPUT: When implemented correctly you should see 10 numbers printed to the console, followed by the text: "Done!"



        //==================================================================================================================================
        //
        // EXERCISE 1.B
        //
        // The observable that receives and emits the data from the remote sensor device uses blocking I/O. These blocking I/O operations
        // are performed on the thread subscribing to the observable. In this case that would be the main thread. You can actually see that
        // it performs blocking I/O because the text "Done!" is printed AFTER the observable completes, meaning the subscribe call was
        // actually blocking the main thread. Obviously that is not what we would like to see in our reactive world, where the sensor data
        // should be processed asynchronously.
        //
        // ASSIGNMENT: Modify the observable chain above in such a way that the I/O scheduler (Schedulers.io()) is used when the blocking
        // I/O operations are performed by the sensor data source observable.
        //
        // HINT: RxJava provides two operators to set a scheduler. Can you explain why one works but the other does not?
        //
        // HINT: You program will most likely exit with just the text "Done!", without printing any sensor data. This is because the RxJava
        // schedulers only create daemon threads. To fix this simply uncomment the blocking sleep statement below.

        // WorkshopUtils.sleep(10000);

        // EXPECTED OUTPUT: When implemented correctly you should see text "Done!" (which is printed after subscribing) BEFORE the sensor
        // data is printed. This proves that the observable chain is now asynchronously emitting data.

    }

}