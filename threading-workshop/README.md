# RxJava 2 Threading workshop

This workshop is meant to get more experience with RxJava 2 in combination with multi-threading.

You can find the exercises in the following folder:

`src/main/java/nl/craftsmen/workshops/rxjavathreading/exercises`

Each exercise contains both the instructions and the assignments as comments in the `run` method.

To run an exercise either launch it from your IDE (as they all contain a main method) or execute the following command line call from a console:

**Windows:**

```shell
run-exercise 1
```

**Linux/Mac:**

```shell
./run-exercise 1
```

_**NOTE:** You might need to make the script executable first: `chmod u+x run-exercise`_