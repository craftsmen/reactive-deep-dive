plugins {
    java
    application
}

group = "nl.craftsmen.workshops"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compile("io.reactivex.rxjava2", "rxjava", "2.2.3")
    testCompile("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}

application {
    mainClassName = "nl.craftsmen.workshops.rxjavathreading.ExerciseLauncher"
}