ECHO OFF

ECHO stopping containers...

docker stop $(docker ps -q --filter ancestor=rweekers/music-network)
docker stop $(docker ps -q --filter ancestor=rweekers/music-client)

ECHO containers stopped...
