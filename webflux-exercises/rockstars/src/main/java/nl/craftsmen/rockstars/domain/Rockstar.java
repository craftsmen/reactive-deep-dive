package nl.craftsmen.rockstars.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Rockstar {

    @Id
    @GeneratedValue
    private Long id;
    private String firstName;
    private String lastName;
    private String greatestHit;

    public Rockstar(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
