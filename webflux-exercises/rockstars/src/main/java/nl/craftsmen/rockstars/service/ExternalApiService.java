package nl.craftsmen.rockstars.service;

import nl.craftsmen.rockstars.domain.Rockstar;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Service
public class ExternalApiService {

    private final RestTemplate restTemplate = new RestTemplate();

    public Rockstar enrichRockstar(Rockstar rockstar) {
        var vars = new HashMap<String, String>();
        vars.put("name", rockstar.getLastName());
        var greatestHit = restTemplate.getForObject("http://localhost:9090/api/artist/{name}", String.class, vars);

        return new Rockstar(rockstar.getId(), rockstar.getFirstName(), rockstar.getLastName(), greatestHit);
    }
}
