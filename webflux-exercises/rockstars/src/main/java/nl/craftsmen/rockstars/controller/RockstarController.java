package nl.craftsmen.rockstars.controller;

import lombok.extern.slf4j.Slf4j;
import nl.craftsmen.rockstars.domain.Rockstar;
import nl.craftsmen.rockstars.repository.RockstarRepository;
import nl.craftsmen.rockstars.service.ExternalApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
@RequestMapping("/api")
public class RockstarController {

    @Autowired
    private RockstarRepository repository;

    @Autowired
    private ExternalApiService externalApiService;

    @CrossOrigin(origins = "http://localhost")
    @GetMapping("/rockstars")
    public List<Rockstar> allRockstars() {
        var rockstars = repository.findAll();
        var enrichedRockstars = rockstars.stream().map(externalApiService::enrichRockstar).collect(toList());
        return enrichedRockstars;
    }

    @GetMapping("/rockstars/{id}")
    public Rockstar findRockstarById(@PathVariable Long id) {
        var rockstar = repository.findById(id).orElseThrow(() -> new RuntimeException("Rockstar not found"));
        return externalApiService.enrichRockstar(rockstar);
    }

    @PostMapping("/rockstars")
    public Rockstar newRockstar(@RequestBody Rockstar newRockstar) {
        return repository.save(newRockstar);
    }

    @PutMapping("/rockstars/{id}")
    public Rockstar replaceRockstar(@RequestBody Rockstar newRockstar, @PathVariable Long id) {

        return repository.findById(id)
                .map(rockstar -> {
                    rockstar.setFirstName(newRockstar.getFirstName());
                    rockstar.setLastName(newRockstar.getLastName());
                    return repository.save(rockstar);
                })
                .orElseGet(() -> {
                    newRockstar.setId(id);
                    return repository.save(newRockstar);
                });
    }

    @DeleteMapping("/rockstars/{id}")
    public void deleteRockstar(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
