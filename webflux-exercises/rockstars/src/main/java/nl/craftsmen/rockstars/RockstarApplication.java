package nl.craftsmen.rockstars;

import lombok.extern.slf4j.Slf4j;
import nl.craftsmen.rockstars.domain.Rockstar;
import nl.craftsmen.rockstars.repository.RockstarRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class RockstarApplication {

	public static void main(String[] args) {
		SpringApplication.run(RockstarApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(RockstarRepository repository) {
		return (args) -> {
			// save a couple of rockstars
			repository.save(new Rockstar("Paul", "Weller"));
			repository.save(new Rockstar("Joe", "Jackson"));
			repository.save(new Rockstar("Van", "Morrison"));
			repository.save(new Rockstar("David", "Byrne"));
			repository.save(new Rockstar("Bryan", "Ferry"));

			// fetch all rockstars
			log.info("Rockstars found with findAll():");
			log.info("-------------------------------");
			for (Rockstar rockstar : repository.findAll()) {
				log.info(rockstar.toString());
			}
			log.info("");

			// fetch an individual rockstar by ID
			repository.findById(1L)
					.ifPresent(rockstar -> {
						log.info("Rockstar found with findById(1L):");
						log.info("--------------------------------");
						log.info(rockstar.toString());
						log.info("");
					});

			// fetch rockstars by last name
			log.info("Rockstar found with findByLastName('Ferry'):");
			log.info("--------------------------------------------");
			repository.findByLastName("Ferry").forEach(ferry -> {
				log.info(ferry.toString());
			});
			log.info("");
		};
	}
}
