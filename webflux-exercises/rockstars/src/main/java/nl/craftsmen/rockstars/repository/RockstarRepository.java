package nl.craftsmen.rockstars.repository;

import nl.craftsmen.rockstars.domain.Rockstar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RockstarRepository extends JpaRepository<Rockstar, Long> {

    List<Rockstar> findByLastName(String lastName);
}