package nl.craftsmen.rockstars;

import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RockstarControllerTests {

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() { RestAssuredMockMvc.webAppContextSetup(context); }

    @Test
    public void testGetSpecificRockstar() {
        given().
            when().
                get("/api/rockstars/{id}", 2).
            then().
                statusCode(200).
                body("id", equalTo(2)).
                body("firstName", equalTo("Joe"));
    }

    @Test
    public void testGetAllRockstars() {
        given().
            when().
                get("/api/rockstars").
            then().
                statusCode(200).
                body("size()", is(5));
    }

    @Test
    public void addRockstar() throws JSONException {
        String body = new JSONObject()
                .put("firstName", "Bob")
                .put("lastName", "Dylan")
                .toString();
        given()
                .body(body)
                .contentType(ContentType.JSON)
            .when()
                .post("/api/rockstars")
            .then()
                .statusCode(200);

    }

    @Test
    public void updateRockstar() throws JSONException {
        String body = new JSONObject()
                .put("firstName", "Robert")
                .put("lastName", "Palmer")
                .toString();
        given()
                .body(body)
                .contentType(ContentType.JSON)
            .when()
                .put("/api/rockstars/{id}", 1)
            .then()
                .statusCode(200);

    }

    @Test
    public void deleteRockstar() {
        given().
            when().
                delete("/api/rockstars/{id}", 1)
            .then()
                .statusCode(200);
    }

}
