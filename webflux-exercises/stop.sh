#!/bin/sh

echo 'stopping containers...'

docker stop $(docker ps -q --filter ancestor=rweekers/music-network)
docker stop $(docker ps -q --filter ancestor=rweekers/music-client)

echo 'containers stopped...'
