#!/bin/sh

echo 'starting containers...'

docker run -d -p 9090:8080 rweekers/music-network

docker run -d -p 80:80 rweekers/music-client

echo 'containers started...'
