package nl.craftsmen.fluxdemo.popstars.controller;

import lombok.extern.slf4j.Slf4j;
import nl.craftsmen.fluxdemo.popstars.repository.PopstarRepository;
import nl.craftsmen.fluxdemo.popstars.service.ExternalApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api")
public class PopstarController {

    @Autowired
    private PopstarRepository repository;

    @Autowired
    private ExternalApiService externalApiService;

    //@CrossOrigin(origins = "http://localhost")
    //@GetMapping("/popstars")
    //public ???<Popstar> allPopstars() {
    //    log.info("Gotten request to get all popstars");
    //    return repository.???;
    //}

    //@GetMapping("/popstars/{id}")
    // public ???<???<Popstar>> findPopstarById(@PathVariable String id) {
    //    return repository.???;
    //}

    //@PostMapping("/popstars")
    //public ???<Popstar> newPopstar(@RequestBody Popstar newPopstar) {
    //    return repository.???;
    //}

    //@PutMapping("/popstars/{id}")
    //public Single<ResponseEntity<Popstar>> replacePopstar(@RequestBody Popstar newPopstar, @PathVariable String id) {
    //   return repository.???
    //}

    //@DeleteMapping("/popstars/{id}")
    //public ??? deletePopstar(@PathVariable String id) {
    // return repository.???;
    //}


}
