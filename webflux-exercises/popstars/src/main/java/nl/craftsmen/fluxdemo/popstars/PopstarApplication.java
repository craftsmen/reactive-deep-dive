package nl.craftsmen.fluxdemo.popstars;

import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import nl.craftsmen.fluxdemo.popstars.domain.Popstar;
import nl.craftsmen.fluxdemo.popstars.repository.PopstarRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.CountDownLatch;

@SpringBootApplication
@Slf4j
public class PopstarApplication {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(PopstarApplication.class, args);
        CountDownLatch latch = new CountDownLatch(1);
        latch.await();
    }

    @Bean
    CommandLineRunner init(PopstarRepository repository) {
        return args -> {
            Flowable<Popstar> popstarFlowable =
                    Flowable.just(
                            new Popstar("1", "Katy", "Perry"),
                            new Popstar("2", "Nicki", "Minaj"),
                            new Popstar("3", "Justin", "Bieber"),
                            new Popstar("4", "Taylor", "Swift"),
                            new Popstar("5", "Ariana", "Grande")
                    );

            repository.saveAll(popstarFlowable).blockingIterable().forEach(i -> log.info("This is popstar {}", i));
        };
    }
}
