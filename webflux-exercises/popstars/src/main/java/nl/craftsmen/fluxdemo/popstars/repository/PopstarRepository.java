package nl.craftsmen.fluxdemo.popstars.repository;

import nl.craftsmen.fluxdemo.popstars.domain.Popstar;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;

public interface PopstarRepository extends RxJava2CrudRepository<Popstar, String> {

}
