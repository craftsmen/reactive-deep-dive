package nl.craftsmen.fluxdemo.popstars.service;

import io.reactivex.Single;
import nl.craftsmen.fluxdemo.popstars.domain.Popstar;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Service
public class ExternalApiService {

    private final RestTemplate restTemplate = new RestTemplate();

    public Single<Popstar> enrichPopstar(Popstar popstar) {
        var vars = new HashMap<String, String>();
        vars.put("name", popstar.getLastName());
        var greatestHit = restTemplate.getForObject("http://localhost:9090/api/artist/{name}", String.class, vars);

        return Single.just(new Popstar(popstar.getId(), popstar.getFirstName(), popstar.getLastName(), greatestHit));
    }
}
