package nl.craftsmen.fluxdemo.popstars.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Popstar {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    private String greatestHit;

    public Popstar(String id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
