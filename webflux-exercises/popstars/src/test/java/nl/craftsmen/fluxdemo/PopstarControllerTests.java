package nl.craftsmen.fluxdemo;

import io.restassured.http.ContentType;
import io.restassured.module.webtestclient.RestAssuredWebTestClient;
import nl.craftsmen.fluxdemo.popstars.PopstarApplication;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.module.webtestclient.RestAssuredWebTestClient.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = PopstarApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PopstarControllerTests {

    @Autowired
    private ApplicationContext context;

    @Before
    public void setup() {
        RestAssuredWebTestClient.applicationContextSetup(context);
    }

    @Test
    public void testGetSpecificPopstar() {
        given().
            when().
                get("/api/popstars/{id}", "3").
            then().
                statusCode(200).
                body("id", equalTo("3")).
                body("firstName", equalTo("Justin"));
    }

    @Test
    public void testGetAllPopstars() {
        given().
            when().
                get("/api/popstars").
            then().
                statusCode(200).
                body("size()", is(5));
    }

    @Test
    public void addPopstar() throws JSONException {
        String body = new JSONObject()
                .put("firstName", "Miley")
                .put("lastName", "Cyrus")
                .toString();
        given()
                .body(body)
                .contentType(ContentType.JSON)
            .when()
                .post("/api/popstars")
            .then()
                .statusCode(200);

    }

    @Test
    public void updatePopstar() throws JSONException {
        String body = new JSONObject()
                .put("firstName", "Bruno")
                .put("lastName", "Mars")
                .toString();
        given()
                .body(body)
                .contentType(ContentType.JSON)
            .when()
                .put("/api/popstars/{id}", "2")
            .then()
                .statusCode(200);

    }

    @Test
    public void deletePopstar() {
        given().
            when().
                delete("/api/popstars/{id}", "1")
            .then()
                .statusCode(200);
    }

}
